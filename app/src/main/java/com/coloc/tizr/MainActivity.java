package com.coloc.tizr;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.coloc.tizr.fragment.Favorite;
import com.coloc.tizr.fragment.HomeFragment;
import com.coloc.tizr.fragment.Search;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView navigationView;
    private int fragmentEnCour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init bottomNav
        navigationView = findViewById(R.id.bottom_navigation);
        initBottomNav();

        //init fragment home
        this.fragmentEnCour = 1;
        loadFragment(new HomeFragment(),0);
    }



    private void initBottomNav() {
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.home) {
                    fragmentEnCour = 1;
                    loadFragment(new HomeFragment(),1);
                }
                else if (id == R.id.favorite) {
                    fragmentEnCour = 2;
                    loadFragment(new Favorite(),1);
                }

                else if (id == R.id.search) {
                    fragmentEnCour = 3;
                    loadFragment(new Search(),1);
                }
                return true;
            }
        });

    }

    // 0 for init, 1 replace
    private void loadFragment(Fragment fragment, int etat) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if (etat == 0 ) {
            ft.add(R.id.container_fragment, new HomeFragment());
        }
        else {
            ft.replace(R.id.container_fragment,fragment);
        }

        ft.addToBackStack("Retour liste film");
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment;
        if (fragmentEnCour==1) {
            fragment = new HomeFragment();
        }
        else if (fragmentEnCour == 2) {
            fragment = new Favorite();
        }
        else {
            fragment = new Search();
        }
        loadFragment(fragment, 1);
    }

}
