package com.coloc.tizr.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coloc.tizr.GetMovieDataService;
import com.coloc.tizr.R;
import com.coloc.tizr.RecyclerViewAdapter;
import com.coloc.tizr.RetrofitInstance;
import com.coloc.tizr.model.Movie;
import com.coloc.tizr.model.MovieCollection;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Search extends Fragment {
    private SearchView searchView;
    private Button annuler;
    private TextView textResearch;
    private ImageView imageResearch;


    RecyclerViewAdapter recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, Movie feature) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            //ft.replace(R.id.nav_host_fragment, new DetailsMovie(feature));
            ft.replace(R.id.container_fragment,new DetailsMovie(feature));
            ft.addToBackStack("Retour liste film");
            ft.commit();
        }});
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewSearch);
        searchView = (SearchView) rootView.findViewById(R.id.search_bar);
        annuler = (Button) rootView.findViewById(R.id.annuler);
        //annuler.setVisibility(View.INVISIBLE);


        this.textResearch = (TextView) rootView.findViewById(R.id.textResearch);
        this.imageResearch = (ImageView) rootView.findViewById(R.id.erreurResearch);
        textResearch.setVisibility(View.INVISIBLE);
        imageResearch.setVisibility(View.INVISIBLE);
        textResearch.setText("Désolé mamène, on n'a rien pour toi");
        imageResearch.setImageResource(R.drawable.ic_sad_green);
        imageResearch.setBackgroundColor(Color.parseColor("#000000"));


        annuler.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                //ft.replace(R.id.nav_host_fragment, new DetailsMovie(feature));
                ft.replace(R.id.container_fragment,new HomeFragment());
                ft.commit();
            }
        });
        // 2. set layoutManger
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));

        // 4. set adapter
        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (annuler.getVisibility() != View.VISIBLE) {
                    annuler.setVisibility(View.VISIBLE);
                }
                fetchMovieData(searchView.getQuery().toString());

                return false;
            }
        });

        return rootView;}

    public void fetchMovieData(String query) {
        final GetMovieDataService GetMovieService = RetrofitInstance.getInstance().create(GetMovieDataService.class);
        GetMovieService.getSearchMovies("1","040fc302130d6412705f5f025c30dbe1", query,"fr-FR" ).enqueue(new Callback<MovieCollection>() {

            @Override
            public void onResponse(Call<MovieCollection> call, Response<MovieCollection> response) {
                if(response.isSuccessful() && response.body() != null) {

                    //Manage data
                    MovieCollection collection = response.body();
                    recyclerAdapter.deleteMovieList();
                    recyclerAdapter.addMovieList(collection.getMovieResult());
                    if( collection.getMovieResult().size() > 0) {
                        textResearch.setVisibility(View.INVISIBLE);
                        imageResearch.setVisibility(View.INVISIBLE);
                    } else {
                        textResearch.setVisibility(View.VISIBLE);
                        imageResearch.setVisibility(View.VISIBLE);
                    }
                    Log.d("SearchCollectionMovie", "msg="+response.body().getMovieResult());
                } else {


                }
            }

            @Override
            public void onFailure(Call<MovieCollection> call, Throwable t) {
                Log.d("SearchCollectionMovie", t.getMessage());
            }
        });
    }
}
