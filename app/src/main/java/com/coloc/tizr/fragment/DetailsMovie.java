package com.coloc.tizr.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.coloc.tizr.GetMovieDataService;
import com.coloc.tizr.R;
import com.coloc.tizr.RetrofitInstance;
import com.coloc.tizr.UseSharedPref;
import com.coloc.tizr.model.Movie;
import com.coloc.tizr.model.TrailerCollection;
import com.muddzdev.styleabletoast.StyleableToast;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsMovie extends Fragment {
    private Movie movie;
    private String API_KEY = "040fc302130d6412705f5f025c30dbe1";

    private View rootView;
    private TextView title;
    private TextView note;
    private TextView description;
    private ImageView image;
    private YouTubePlayerView youTubePlayerView;
    private Button addFavorite;
    private String textToast;




    public DetailsMovie() {
    }

   public DetailsMovie(Movie feature) {
        movie = feature;
    }

    public static DetailsMovie newInstance() {
        return new DetailsMovie();
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_movie, container, false);
        youTubePlayerView = rootView.findViewById(R.id.youtube_player_view);
        addFavorite = rootView.findViewById(R.id.btnFavoris);
        if(movie.getFavorite()) addFavorite.setText("Retirer des favoris");
        addFavorite.setOnClickListener(addFavoriteClick);
        getLifecycle().addObserver(youTubePlayerView);
        getTrailer(movie.getId());
        setDetailsMovie();

        return rootView;
    }




    // bouton favoris
private View.OnClickListener addFavoriteClick = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
       try {
            setTextAddFavorite(movie);
            StyleableToast toast = StyleableToast.makeText(getContext(),textToast, R.style.toastPerso);
           toast.setGravity(Gravity.CENTER);
           toast.show();

      } catch (Exception e) {
            Toast.makeText(getActivity(), (String)"Il y a une erreur MAMENE",
                    Toast.LENGTH_LONG).show();
       }
    }
};


   public void setDetailsMovie() {
        title = (TextView) rootView.findViewById(R.id.titleMovie);
        title.setText(movie.getTitle());
        description = (TextView) rootView.findViewById(R.id.descriptionMovie);
        description.setText(movie.getOverview());
        image = (ImageView) rootView.findViewById(R.id.afficheMovie);
        Picasso.get().load("http://image.tmdb.org/t/p/w92/" + movie.getPosterPath()).into(image);
        note = (TextView) rootView.findViewById(R.id.noteMovie);
        note.setText("La note TIZ'R : " + Double.toString((double) movie.getVoteAverage()) + " / 10");

    }

    private void getTrailer(int movieId) {
        GetMovieDataService movieTrailerService = RetrofitInstance.getInstance().create(GetMovieDataService.class);
        Call<TrailerCollection> call = movieTrailerService.getTrailers(movieId, API_KEY);


        call.enqueue(new Callback<TrailerCollection>() {
            @Override
            public void onResponse(Call<TrailerCollection> call, final Response<TrailerCollection> response) {
                youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                        if(response.body().getTrailerResult().size() > 0) {
                            String videoId = response.body().getTrailerResult().get(0).getKey();
                            youTubePlayer.loadVideo(videoId, 0f);
                        }
                        else {
                            youTubePlayerView.setVisibility(View.GONE);
                            final ImageView imageView = rootView.findViewById(R.id.erreurVideoImg);
                            final TextView textView = rootView.findViewById(R.id.erreurVideoText);
                            imageView.setImageResource(R.drawable.ic_sad);
                            textView.setText("On ne l'a pas respecté mamène, on n'a aucun trailer pour ce film");
                            textView.setTextColor(Color.parseColor("#CC0000"));
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<TrailerCollection> call, Throwable t) {
                System.out.println("erreur");
            }
        });
    }

    private void setTextAddFavorite(Movie movie) {
        UseSharedPref useSharedPref = new UseSharedPref(getContext());


        if(!movie.getFavorite()) {
            addFavorite.setText("Retirer des    Favoris");
            textToast = "C'EST AJOUTE L'ARTISTE";
            movie.setFavorite();
            useSharedPref.setID(String.valueOf(movie.getId()));
            useSharedPref.setTitle(movie.getTitle());
            useSharedPref.setOverview(movie.getOverview());
            useSharedPref.setPosterPath(movie.getPosterPath());
        }
        else {
            addFavorite.setText("Ajouter aux Favoris");
            textToast = "C'EST RETIRER L'ARTISTE";
            movie.setFavorite();
            useSharedPref.removeMovie(movie);
        }
    }


}
