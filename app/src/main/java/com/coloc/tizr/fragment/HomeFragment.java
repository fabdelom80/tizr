package com.coloc.tizr.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.coloc.tizr.R;
import com.coloc.tizr.ViewPagerAdapter;
import com.coloc.tizr.fragment.PopularFragment;
import com.coloc.tizr.fragment.TopFragment;
import com.coloc.tizr.fragment.UpcomingFragment;
import com.google.android.material.tabs.TabLayout;

public class HomeFragment extends Fragment {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        mTabLayout = (TabLayout) getView().findViewById(R.id.activity_main_tabs);
        mViewPager = (ViewPager) getView().findViewById(R.id.activity_main_viewpager);

        adapter.addFragment(new PopularFragment(), "POPULAIRE");
        adapter.addFragment(new UpcomingFragment(), "A VENIR");
        adapter.addFragment(new TopFragment(), "TOP TIZ'R");

        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
    }

}
