package com.coloc.tizr.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coloc.tizr.R;
import com.coloc.tizr.RecyclerViewAdapter;
import com.coloc.tizr.UseSharedPref;
import com.coloc.tizr.model.Movie;

import java.util.ArrayList;
import java.util.List;


public class Favorite extends Fragment {
    private View rootView;
    private List<Movie> movieList = new ArrayList<Movie>();;

    RecyclerViewAdapter recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, Movie feature) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            //ft.replace(R.id.nav_host_fragment, new DetailsMovie(feature));
            ft.replace(R.id.container_fragment,new DetailsMovie(feature));
            ft.addToBackStack("Retour liste film");
            ft.commit();
        }});



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favorite, container, false);
        UseSharedPref useSharedPref = new UseSharedPref(getContext());
        movieList = useSharedPref.getListMovie();

        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewFav);

        // 2. set layoutManger
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));

        // 4. set adapter
        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);

        //5.  add data
        recyclerAdapter.addMovieList(movieList);


        return rootView;
    }





}
