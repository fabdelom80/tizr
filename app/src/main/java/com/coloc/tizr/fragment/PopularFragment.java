package com.coloc.tizr.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coloc.tizr.GetMovieDataService;
import com.coloc.tizr.R;
import com.coloc.tizr.RecyclerViewAdapter;
import com.coloc.tizr.RetrofitInstance;
import com.coloc.tizr.UseSharedPref;
import com.coloc.tizr.model.Movie;
import com.coloc.tizr.model.MovieCollection;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopularFragment extends Fragment {


    private int iPage = 1;

    public static PopularFragment newInstance() {
        return (new PopularFragment());
    }

    RecyclerViewAdapter recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, Movie feature) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.homeMovie_fragment,new DetailsMovie(feature));
            ft.addToBackStack("Retour liste film"); //retour a cette page
            ft.commit();
        }});

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        final View rootView = inflater.inflate(R.layout.fragment_popular, container, false);
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewPop);
        // 2. set layoutManger
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));

        // 4. set adapter
        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {

                } else {

                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    iPage++;
                    fetchMovieData(iPage);
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                } else {

                }
            }
        });

        //5.  add data
        fetchMovieData(iPage);

        return rootView;}



    public void fetchMovieData(int page) {
        final GetMovieDataService GetMovieService = RetrofitInstance.getInstance().create(GetMovieDataService.class);
        GetMovieService.getPopularMovies(String.valueOf(page),"040fc302130d6412705f5f025c30dbe1", "fr-FR").enqueue(new Callback<MovieCollection>() {

            @Override
            public void onResponse(Call<MovieCollection> call, Response<MovieCollection> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage data
                    MovieCollection collection = response.body();
                    recyclerAdapter.addMovieList(collection.getMovieResult());
                    UseSharedPref useSharedPref = new UseSharedPref(getContext());
                    useSharedPref.getMovieFavorite(collection.getMovieResult());
                    Log.d("CollectionMovie", "msg="+response.body().getMovieResult());
                } else {
                    //Toast.makeText( "Oups", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MovieCollection> call, Throwable t) {
                Log.d("CollectionMovie", t.getMessage());
            }
        });
    }
}
