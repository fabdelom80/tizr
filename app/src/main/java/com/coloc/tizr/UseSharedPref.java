package com.coloc.tizr;

import android.content.Context;
import android.content.SharedPreferences;

import com.coloc.tizr.model.Movie;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class UseSharedPref {

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public UseSharedPref(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences("Fav", Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }


    public void setID(String id) {
        Set<String> setFavId = getId();
        setFavId.add(id);
        editor.putStringSet("id", setFavId);
        editor.commit();
    }


    public void setTitle(String title) {
        Set<String> setFavTitle = getTitle();
        setFavTitle.add(title);
        editor.putStringSet("title", setFavTitle);
        editor.commit();
    }

    public void setPosterPath(String posterPath) {
        Set<String> setFavPosterPath = getPosterPath();
        setFavPosterPath.add(posterPath);
        editor.putStringSet("posterPath", setFavPosterPath);
        editor.commit();
    }

    public void setOverview(String overview) {
        Set<String> setFavOverview = getOverview();
        setFavOverview.add(overview);
        editor.putStringSet("overview", setFavOverview);
        editor.commit();
    }

    public Set<String> getId() {
      Set<String> getId = new HashSet();
      if(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("id", null) != null) {
          getId.addAll(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("id", null));
      }
      return getId;
    }

    public Set<String> getTitle() {
        Set<String> getTitle = new HashSet();
        if(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("title", null) != null) {
            getTitle.addAll(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("title", null));
        }
        return getTitle;
    }

    public Set<String> getPosterPath() {
        Set<String> getPosterPath = new HashSet();
        if (context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("posterPath", null) != null) {
            getPosterPath.addAll(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("posterPath", null));
        }
        return getPosterPath;
    }

    public Set<String> getOverview() {
        Set<String> getOverview = new HashSet();
        if (context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("overview", null) != null) {
            getOverview.addAll(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("overview", null));
        }
        return getOverview;
    }

    public List<Movie> getListMovie() {
        List<String> title = new ArrayList<String>();
        List<String> id = new ArrayList<String>();
        List<String> overview = new ArrayList<String>();
        List<String> posterPath = new ArrayList<String>();
        if(getId() != null) {
            title.addAll(getTitle());
            id.addAll(getId());
            overview.addAll(getOverview());
            posterPath.addAll(getPosterPath());
        }

        List<Movie> movies = new ArrayList<Movie>();
        for (int i = 0; i < title.size(); i++) {
            Movie movie = new Movie(Integer.parseInt(id.get(i)), title.get(i), posterPath.get(i), overview.get(i));
            movies.add(movie);
        }
        return movies;
    }


    public void getMovieFavorite(List<Movie> movieList) {
        Set<String> getId = new HashSet();
        if(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("id", null) != null) {
            getId.addAll(context.getSharedPreferences("Fav", Context.MODE_PRIVATE).getStringSet("id", null));
        }
        for (int i = 0; i < movieList.size(); i++) {
            for (Iterator<String> it = getId.iterator(); it.hasNext(); ) {
                String string = it.next();
                if (string.trim().equals(String.valueOf(movieList.get(i).getId()))) {
                    movieList.get(i).setFavorite(true);
                }
            }

        }
    }

    public void removeMovie(Movie movie) {
        Set<String> setFav = getId();
        setFav.remove(String.valueOf(movie.getId()));
        editor.putStringSet("id",setFav);

        setFav = getTitle();
        setFav.remove(movie.getTitle());
        editor.putStringSet("title",setFav);

        setFav = getOverview();
        setFav.remove(movie.getOverview());
        editor.putStringSet("overview",setFav);

        setFav = getPosterPath();
        setFav.remove(movie.getPosterPath());
        editor.putStringSet("posterPath",setFav);

        editor.commit();
    }
}
