package com.coloc.tizr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TrailerCollection implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("results")
    private List<Trailer> trailerResult;

    public TrailerCollection(int id, ArrayList<Trailer> trailerResult) {
        this.id = id;
        this.trailerResult = trailerResult;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Trailer> getTrailerResult() {
        return trailerResult;
    }

    public void setTrailerResult(List<Trailer> trailerResult) {
        this.trailerResult = trailerResult;
    }
}
