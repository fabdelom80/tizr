package com.coloc.tizr;


import com.coloc.tizr.model.MovieCollection;
import com.coloc.tizr.model.TrailerCollection;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetMovieDataService {
    @GET("movie/popular")
    Call<MovieCollection> getPopularMovies(@Query("page") String page, @Query("api_key") String userkey, @Query("language") String langage);

    @GET("movie/top_rated")
    Call<MovieCollection> getTopRatedMovies(@Query("page") String page, @Query("api_key") String userkey, @Query("language") String langage);

    @GET("movie/{id}/videos")
    Call<TrailerCollection> getTrailers(@Path("id") int movieId, @Query("api_key") String userkey);

    @GET("/3/search/movie")
    Call<MovieCollection> getSearchMovies(@Query("page") String page, @Query("api_key") String userkey, @Query("query") String query, @Query("language") String langage);

    @GET("movie/upcoming")
    Call<MovieCollection> getUpcomingMovie(@Query("page") String page, @Query("api_key") String userkey, @Query("language") String langage);
}