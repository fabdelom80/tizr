package com.coloc.tizr;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.coloc.tizr.model.Movie;
import com.coloc.tizr.model.Trailer;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Movie> movieList;
    private List<Trailer> trailerList;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View view, Movie feature);
    }

    public RecyclerViewAdapter(List<Movie> movieList, OnItemClickListener listener) {
        this.movieList = movieList;
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return this.movieList.size();
    }

    public void addMovieList(List<Movie> movieList) {
        this.movieList.addAll(movieList);
        notifyDataSetChanged();
    }

    public void deleteMovieList() {
        this.movieList.clear();
        notifyDataSetChanged();
    }

    public void addTrailerList(List<Trailer> trailerList) {
        this.trailerList.addAll(trailerList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.affiche_movie, parent, false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Movie movie = movieList.get(position);
        if(movie.getPosterPath() != null) {
            Picasso.get().load("http://image.tmdb.org/t/p/w92/"+movie.getPosterPath()).into(holder.image);
            holder.textPasDimage.setVisibility(View.INVISIBLE);
        } else {
            holder.image.setImageResource(R.drawable.splash);
            holder.image.setBackgroundColor(Color.parseColor("#000000"));
            holder.textPasDimage.setText(movie.getTitle());
        }

        if(movie.getFavorite()) {
            holder.coeurFavoris.setImageResource(R.drawable.ic_favorite);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, movie);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final ImageView image;
        final ImageView coeurFavoris;
        final TextView textPasDimage;

        ViewHolder(View view) {
            super(view);
            itemView = view;
            image = view.findViewById(R.id.affiche);
            coeurFavoris = view.findViewById(R.id.coeurFavoris);
            textPasDimage = view.findViewById(R.id.titrePasDaffiche);
        }
    }
}
